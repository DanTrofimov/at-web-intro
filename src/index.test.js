/**
 * @jest-environment jsdom
 */

const fs = require('fs');
const path = require('path');
const html = fs.readFileSync(path.resolve(__dirname, './index.html'), 'utf8');

jest.dontMock('fs');

describe("Тесты, 1-ая лекция", () => {

    beforeEach(() => {
        document.documentElement.innerHTML = html.toString();
    });

    describe("Селекторы", () => {
        test("Поле для ввода email в форме входа содержит плейсхолдер example@gmail.com", () => {
            const placeholder = document; // добавить нужный селектор
            expect(placeholder).toBe("example@gmail.com");
        })

        test("Последний фремворк в списке во второй статье - React", () => {
            const listLastItem = document; // добавить нужный селектор
            expect(listLastItem).toBe("React");
        })
    })

    describe("JS, массивы, объекты, функции", () => {
        test("Средний возраст разработчика = 28", () => {
            const anrey = { name: "Андрей", age: 25 };
            const petya = { name: "Петя", age: 30 };
            const masha = { name: "Маша", age: 29 };

            const developers = [anrey, petya, masha];
            const averageAge = developers.reduce((sum, developer) => sum += developer.age, 0) / 3; // найти используя методы массивов
                
            expect(averageAge).toBe(28);
        })
        
        test("Сумма зарплат = 390", () => {
            let salaries = {
                John: 100,
                Ann: 160,
                Pete: 130
            };

            const sum = 0 // найти сумму зарплат

            expect(sum).toBe(390);
        })

        test("Преобразует массив чисел в массив факториалов", () => {
            const nums = [1, 2, 3, 4, 5];

            const factorial = (num) => {
                // реализовать функцию
            };

            const factorials = nums.map(num => factorial(num));

            expect(factorials).toBe([1, 2, 6, 24, 120])
        })
    })
})
